package apvereda.twittertt;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alejandro Perez Vereda
 *
 */
public class TwitterTT
{
    public static void main(String[] args) {

        // STEP 1: argument checking
        if (args.length == 0) {
            throw new RuntimeException("The number of args is 0. Usage: "
                    + "SparkTwitterTT fileOrDirectoryName") ;
        }

        // STEP 2: create a SparkConf object
        SparkConf conf = new SparkConf().setAppName("SparkTwitterTT") ;

        // STEP 3: create a Java Spark context
        JavaSparkContext sparkContext = new JavaSparkContext(conf) ;

        // STEP 4: read the lines from the file(s)
        JavaRDD<String> lines = sparkContext.textFile(args[0]) ;

        // STEP 5: map the entire tweet in the message and the user
        JavaRDD<String> messages = lines.map(s -> Arrays.asList(s.split("\t")).get(2));
        JavaRDD<String> users = lines.map(s -> Arrays.asList(s.split("\t")).get(1));

        // STEP 6: obtain the trending topic words
        List<Tuple2<Integer, String>> TTwords  = messages.flatMap(s -> Arrays.asList(s.split(" ")).iterator())
                .filter(s -> !s.equals("RT") && !s.equals("http") && s.length() > 2)
                .mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKey((a, b) -> a + b )
                .mapToPair(s -> s.swap())
                .sortByKey(false)
                .take(10);

        // STEP 7: obtain the most active user
        Tuple2<Integer, String> mostActiveUser = users.mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKey((a, b) -> a + b )
                .mapToPair(s -> s.swap())
                .sortByKey(false)
                .first();

        // STEP 8: obtain the shortest tweet
        messages = lines.map(s -> Arrays.asList(s.split("\t")).get(2));
        Tuple2<Integer, String> shortestTweet = messages.mapToPair(s -> new Tuple2<>(s.length(), s))
                .sortByKey()
                .first();

        // STEP 9: print the result
        try {
            FileWriter salida1 = new FileWriter("salida/salida1.txt");
            FileWriter salida2 = new FileWriter("salida/salida2.txt");
            FileWriter salida3 = new FileWriter("salida/salida3.txt");

            salida1.write("Trending Topics:\n");
            for (Tuple2<Integer, String> tuple : TTwords) {
                salida1.write(tuple._2() + ": " + tuple._1() + " mentions\n");
            }

            salida2.write("The most active user is " + mostActiveUser._2
                    + " and the number of tweets she wrote is " + mostActiveUser._1 + " \n");

            String author = lines.filter(s -> s.contains(shortestTweet._2))
                    .map(s -> Arrays.asList(s.split("\t")).get(1)).first();
            String date = lines.filter(s -> s.contains(shortestTweet._2))
                    .map(s -> Arrays.asList(s.split("\t")).get(3)).first();
            salida3.write("The shortest tweet was written by " + author + " on " + date
                    + " and its length is " + shortestTweet._1);

            salida1.close();
            salida2.close();
            salida3.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // STEP 10: stop de spark context
        sparkContext.stop();
    }
}
